package com.example.tabsdemo.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
    private val _name = MutableLiveData<String>()
    private val _temp = MutableLiveData<String>()
    private val _desc = MutableLiveData<String>()
    private val _ws = MutableLiveData<String>()
    private val _wd = MutableLiveData<String>()

    val text: LiveData<String> = Transformations.map(_name) {
        "city: $it"
    }

    val text1: LiveData<String> = Transformations.map(_temp) {
        "Cur temp: $it"
    }

    val text2: LiveData<String> = Transformations.map(_desc) {
        "$it"
    }

    val text3: LiveData<String> = Transformations.map(_ws) {
        "Wind speed: $it"
    }

    val text4: LiveData<String> = Transformations.map(_wd) {
        "Wind direction: $it"
    }



    fun setIndex(index: Int) {
        _index.value = index
    }

    fun setName(name: String) {
        _name.value = name
    }

    fun setTemp(temp: String) {
        _temp.value = temp
    }
    fun setDesc(desc: String) {
        _desc.value = desc
    }
    fun setWS(ws: String) {
        _ws.value = ws
    }
    fun setWD(wd: String) {
        _wd.value = wd
    }
}