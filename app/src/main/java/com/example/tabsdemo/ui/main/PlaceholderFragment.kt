package com.example.tabsdemo.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tabsdemo.R
import com.example.tabsdemo.Weather
import com.example.tabsdemo.databinding.FragmentMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.InputStream
import java.net.URL
import java.util.*

// TODO: создать фрагмент со сведениями о погоде
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel
    private var _binding: FragmentMainBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
//            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
//        }

        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setName(arguments?.getString(NAME) ?: "-")
        }
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setTemp(arguments?.getString(TEMP) ?: "-")
        }
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setDesc(arguments?.getString(DESC) ?: "-")
        }
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setWS(arguments?.getString(WS) ?: "-")
        }
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setWD(arguments?.getString(WD) ?: "-")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val root = binding.root

        val textView: TextView = binding.sectionLabel
        val textView1: TextView = binding.temp
        val textView2: TextView = binding.desc
        val textView3: TextView = binding.windSpeed
        val textView4: TextView = binding.windDirection

        val city = arguments?.getString(NAME)
        GlobalScope.launch(Dispatchers.IO) {
            var weather = loadWeather(city?: "-")
            GlobalScope.launch (Dispatchers.Main) {
                textView1.text = "Cur. temp: " + weather.temp
                textView2.text = weather.desc
                textView3.text = "Wind speed: " + weather.wind_speed
                textView4.text = "Wind direction: " + weather.wind_direction
            }
        }


        pageViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        return root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val TEMP = "temp"
        private const val DESC = "desc"
        private const val WS = "ws"
        private const val WD = "wd"
        private const val NAME = "name"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int, sectionName: String, temp: String, desc: String, ws: String, wd: String): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putString(NAME, sectionName)
                    putString(TEMP, temp)
                    putString(DESC, desc)
                    putString(WS, ws)
                    putString(WD, wd)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun loadWeather(city: String): Weather {
        Log.d("mytag", "loadWeather start")

        val key = context?.resources?.getString(R.string.API)
        val weatherURL = "http://api.weatherapi.com/v1/current.json?key=$key&q=$city&aqi=no"
        Log.d("mytag", "loadWeather before stream")

        val stream = URL(weatherURL).getContent() as InputStream
        Log.d("mytag", "loadWeather before read")

        val data = Scanner(stream).nextLine()
        Log.d("mytag", "loadWeather after read")

        val json = JSONObject(data).getJSONObject("current")
        val temp = json.getString("temp_c")
        val desc = json.getJSONObject("condition").getString("text")
        val wind_speed = json.getString("wind_kph")
        val wind_direction = json.getString("wind_dir")
        Log.d("mytag", "loadWeather end")

        return Weather(city, temp, desc, wind_speed, wind_direction)

    }
}