package com.example.tabsdemo.ui.main

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.tabsdemo.R
import com.example.tabsdemo.Weather
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.InputStream
import java.net.URL
import java.util.*


class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {
    lateinit var weather: Weather
    override fun getItem(position: Int): Fragment {
        // TODO: создать фрагмент со сведениями о погоде
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Log.d("mytag", "get item: ${context.resources.getStringArray(R.array.cities)[position]}")
        val city = context.resources.getStringArray(R.array.cities)[position]
//        val w = loadWeather(context.resources.getStringArray(R.array.cities)[position])
        var temp = ""; var desc = ""; var ws = ""; var wd = ""
        Log.d("mytag", "get_weather")


        return PlaceholderFragment.newInstance(position + 1, context.resources.getStringArray(R.array.cities)[position], temp, desc, ws, wd)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        // TODO: задать заголовок - название города
        Log.d("mytag", "getPageTitle: ${context.resources.getStringArray(R.array.cities)[position]}")
        return context.resources.getStringArray(R.array.cities)[position]
    }

    override fun getCount(): Int {
        // TODO: возвращать число доступных городов
        return context.resources.getStringArray(R.array.cities).size
    }


}