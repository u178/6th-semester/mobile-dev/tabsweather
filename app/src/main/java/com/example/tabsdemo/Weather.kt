package com.example.tabsdemo

data class Weather(
    var name: String,
    var temp: String,
    var desc: String,
    var wind_speed: String,
    var wind_direction: String
) {
}